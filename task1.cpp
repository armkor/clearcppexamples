#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Car
{
public:
    Car(int weight, int volume);
    bool operator <(Car& other);

    int weightСapacity;
    int volumeСapacity;
};


Car::Car(int weight, int volume)
    : weightСapacity(weight)
    , volumeСapacity(volume)
{
}

bool Car::operator <(Car& other)
{
    return weightСapacity < other.weightСapacity;
}

int main(int argc, char *argv[])
{
   vector<Car> v;

   srand(time(NULL));

   for (int i = 0; i < 10; i++)
   {
       v.push_back(Car(rand() % 100, rand() % 100));
   }

    for (auto iv: v) {
        std::cout << iv.weightСapacity << " ";
    }

    std::cout << std::endl;

    std::sort(v.begin(), v.end());

    for (auto iv : v) {
        std::cout << iv.weightСapacity << " ";
    }
    return 0;
}
