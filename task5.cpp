#include <typeinfo>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Load
{
public:
    Load(int vol = 10, int wgth = 10)
        : volume(vol)
        , weight(wgth)
    {}

    void print()
    {
        cout << volume <<  "," << weight << endl;
    }

    int volume;
    int weight;
};


class Car
{
public:
    Car(int weight = 20, int volume = 20)
        : weightСapacity(weight)
        , volumeСapacity(volume)
    {
    }

    void print()
    {
        cout << weightСapacity <<  "," << volumeСapacity << endl;
    }

    int weightСapacity;
    int volumeСapacity;
};

template<class T>
class AddPrintName // определение шаблона
{
public:
  AddPrintName(T elem_):elem(elem_){}
  void printFull()
  {
      cout << typeid(elem).name();
      elem.print();
  }
private:
  T elem;
};

int main(int argc, char *argv[])
{
   {
       vector<AddPrintName<Car>> v;

       int i = 0;
       while ( i < 10)
       {
           v.push_back(AddPrintName<Car>(Car()));
           i++;
       }

       for(auto vt: v)
       {
           vt.printFull();
       }
   }

   cout << endl;

   {
       vector<AddPrintName<Load>> v;

       int i = 0;
       while ( i < 10)
       {
           v.push_back(AddPrintName<Load>(Load()));
           i++;
       }

       for(auto vt: v)
       {
           vt.printFull();
       }

   }

   return 0;
}
