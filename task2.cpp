#include <iostream>
#include <list>
#include <algorithm>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Load
{
public:
    Load(int vol, int wgth)
        : volume(vol)
        , weight(wgth)
    {}
    int volume;
    int weight;
};

class Car
{
public:
    virtual ~Car(){}
    virtual bool canLoad(Load &load){ return false; }
    virtual void print(){}
};


class CarWeightVolume: public Car
{
public:
    CarWeightVolume(int weight, int volume);
    virtual bool canLoad(Load &load) override;
    virtual void print() override;

private:
    int weightСapacity;
    int volumeСapacity;
};


class CarWeight: public Car
{
public:
    CarWeight(int weight);
    virtual bool canLoad(Load &load) override;
    virtual void print() override;

private:
    int weightСapacity;
};

CarWeightVolume::CarWeightVolume(int weight, int volume)
    : weightСapacity(weight)
    , volumeСapacity(volume)
{
}

bool CarWeightVolume::canLoad(Load &load)
{
    return load.weight <= weightСapacity && load.volume <= volumeСapacity;
}

void CarWeightVolume::print()
{
    cout << "Car with weight:" << weightСapacity << " volume:" << volumeСapacity << std::endl;
}

CarWeight::CarWeight(int weight)
    : weightСapacity(weight)
{
}

bool CarWeight::canLoad(Load &load)
{
    return load.weight <= weightСapacity;
}

void CarWeight::print()
{
    cout << "Car with weight:" << weightСapacity << std::endl;
}

int main(int argc, char *argv[])
{
   list<Car*> v;

   srand (time(NULL));

   for (int i = 0; i < 5; i++)
   {
       v.push_back(new CarWeightVolume(rand() % 100, rand() % 100));
   }

   for (int i = 5; i < 10; i++)
   {
       v.push_back(new CarWeight(rand() % 100));
   }

   std::cout << "Container contains:" << std::endl;

   for (auto iv: v) {
       iv->print();
   }

   std::cout << std::endl;

   Load load(rand() % 100, rand() % 100);
   std::cout << "Need to replace Load: weigth" << load.weight << " volume:" << load.volume << std::endl;

   for (auto it = v.begin(); it != v.end();)
   {
       if(!((*it)->canLoad(load)))
       {
           delete *it;
           it = v.erase(it);
       }
       else
       {
           ++it;
       }
   }

   std::cout << "Cars that can contain load:" << std::endl;
   for (auto iv: v) {
       iv->print();
   }

   for (auto it = v.begin(); it != v.end();)
   {
       delete *it;
       it = v.erase(it);
   }

   return 0;
}
