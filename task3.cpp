#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int op_increase (int i) { return ++i; }

int main(int argc, char *argv[])
{
   vector<int> v(10, 2);

   for (auto iv : v) {
       std::cout << iv << " ";
   }

   std::transform(v.begin(), v.end(), v.begin(), op_increase);

   std::cout << endl;

   for (auto iv : v) {
       std::cout << iv << " ";
   }

   return 0;
}