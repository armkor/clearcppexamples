#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Load
{
public:
    Load(int vol = 10, int wgth = 10)
        : volume(vol)
        , weight(wgth)
    {}

    Load& operator+=(int value){

          this->volume += value;
          this->weight += value;
          return *this;
    }

    int volume;
    int weight;
};


class Car
{
public:
    Car(int weight = 20, int volume = 20)
        : weightСapacity(weight)
        , volumeСapacity(volume)
    {
    }

    Car& operator+=(int value){

          this->volumeСapacity += value;
          this->weightСapacity += value;
          return *this;
    }

    int weightСapacity;
    int volumeСapacity;
};

template < typename T >
void increase ( T& elem)
{
    elem += 1;
}

int main(int argc, char *argv[])
{
   {
       vector<int> v(10, 2);

         for (auto iv : v) {
             std::cout << iv << " ";
         }

         std::cout << endl;

         for (auto& iv : v) {
             increase(iv);
         }

         for (auto& iv : v) {
             std::cout << iv << " ";
         }
   }

   std::cout << endl;

   {
       vector<Load> v(10);

         for (auto iv : v) {
             std::cout << iv.volume  << " " << iv.weight <<" , ";
         }

         std::cout << endl;

         for (auto& iv : v) {
             increase(iv);
         }

         for (auto& iv : v) {
             std::cout << iv.volume  << " " << iv.weight <<" , ";
         }
   }

   std::cout << endl;


   {
       vector<Car> v(10);

         for (auto iv : v) {
             std::cout << iv.weightСapacity  << " " << iv.volumeСapacity <<" , ";
         }

         std::cout << endl;

         for (auto& iv : v) {
             increase(iv);
         }

         for (auto& iv : v) {
             std::cout << iv.weightСapacity  << " " << iv.volumeСapacity <<" , ";
         }
   }

   return 0;
}
