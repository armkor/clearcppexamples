Задания по написанию кода на С++.
В каждом из заданий надо написать код на языке С++ с использованием STL и/или
BOOST при необходимости, который моделирует ситуацию и решает поставленную
задачу. То есть в коде должны быть определены нужные классы и переменные,
заполнены какими-то примерами значений и написан код функции, решающей
поставленную задачу. * отмечены сложные задачи, их можно не решать.

Задание №1:
Есть набор машин с двумя характеристиками: вместимость по весу и вместимость по объему.
Расставьте машины в наборе в порядке убывания (приоритет у первой характеристики).

Задание №2:
Есть ОДИН набор с двумя разновидностями машин, у одних есть две характеристики: вместимость по весу и
вместимость по объему, а у других только вместимость по весу (нет ограничения по объему). Есть груз с
заданным весом и объемом.
Удалите из набора те машины, в которые не влезет груз, используя в решении полиморфизм.

Задание №3:
Есть набор целых чисел.
Напишите функцию увеличения всех чисел набора на единицу без использования циклов.

Задание №4:
Есть набор целых чисел, набор машин (с двумя параметрами: вместимость по весу и объему), набор грузов (с
двумя параметрами: вес и объем).
Напишите ОДНУ шаблонную функцию увеличения всех чисел набора или всех параметров всех объектов
набора на единицу.

Задание №5:
Есть класс, определяющий машину (с двумя параметрами: вместимость по весу и объему) и класс,
определяющий груз (с двумя параметрами: вес и объем), оба класса имеют метод print() для вывода
значений атрибутов на консоль .
Напишите ОДИН шаблонный класс, который расширял бы свойства объекта атрибутом «название» и добавлял
бы в логику печати на консоль вывод этого названия. С помощью этого шаблонного класса надо создать
объекты типа «машина с названием» и «груз с названием» и вывести их на печать.